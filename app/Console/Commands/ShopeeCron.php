<?php

namespace App\Console\Commands;

use App\Models\Shopee\QrTransaction;
use App\Models\TransactionV2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Ramsey\Uuid\Uuid;

class ShopeeCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopee:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $bad_datas = [];
        $good_datas = [];
        $pending_transactions = TransactionV2::where('state', 2)
            ->where('channel', 'SHOPEEPAY')
            ->whereDate('created_at', '<=', '2021-09-30')
            ->get();
        echo 'There are '. count($pending_transactions) . ' is requested data' . "\n";
        foreach ($pending_transactions as $pending_transaction) {
            $is_good_data = false;
            $vendor_id = $pending_transaction->vendor_trx_id;
            $transaction_type = $pending_transaction->transaction_type;
            $qr_transaction = null;
            if ($transaction_type == "qr") {
                $qr_transaction = QrTransaction::where('payment_reference_id', $vendor_id)
                    ->where('transaction_code', 'CREATE_QR')
                    ->first();
            } else if ($transaction_type == "jumpapp") {
                $qr_transaction = QrTransaction::where('payment_reference_id', $vendor_id)
                    ->where('transaction_code', 'CREATE_PAYMENT')
                    ->first();
            }

            if ($qr_transaction == null) {
                array_push($bad_datas, $pending_transaction->id);
                $pending_transaction->state = 7;
                $pending_transaction->save();
                echo $pending_transaction->id . " is inserted to BAD data because there is no payment detected\n";
                continue;
            }

            $create_request_log = json_decode($qr_transaction->request_payload);

            //check status to shopee
            $shopee_request = [
                'request_id' => Uuid::uuid1(),
                'payment_reference_id' => $vendor_id,
                'merchant_ext_id' => $create_request_log->merchant_ext_id,
                'store_ext_id' => $create_request_log->store_ext_id
            ];

            $signature = $this->generate_signature($shopee_request, "uloaDp6belhOBZoRjNgKroqYH2KNLXXC");

            echo "\n\nCheck status request: \n";
            echo json_encode($shopee_request);

            $response = Http::withHeaders([
                "X-Airpay-ClientId" => "MC Payment",
                "X-Airpay-Req-H" => $signature,
            ])
                ->withUserAgent("PostmanRuntime/7.26.8")
                ->contentType("application/json")
                ->post( 'https://api.wallet.airpay.co.id/v3/merchant-host/transaction/payment/check', $shopee_request);

            $check_payment_response = json_decode($response->body());

            echo "\nCheck status result: \n";
            echo $response->body();

            echo "\n";

            if ($check_payment_response->debug_msg == "success" && $check_payment_response->payment_status == 1) {
                array_push($good_datas, $pending_transaction->id);
                $pending_transaction->state = 1;
                echo $pending_transaction->id . " is inserted to GOOD data because there is payment detected\n";
            } else {
                array_push($bad_datas, $pending_transaction->id);
                $pending_transaction->state = 7;
                echo $pending_transaction->id . " is inserted to BAD data because there is no payment detected\n";
            }
            $pending_transaction->save();
        }

        echo "Bad data: \n";
        foreach ($bad_datas as $bad_data) {
            echo $bad_data .",";
        }
        echo "\n\nTotal bad data: ". count($bad_datas);
        echo "\n\nGood data: \n";
        foreach ($good_datas as $good_data) {
            echo $good_data . ",";
        }
        echo "\n\nTotal good data: ". count($good_datas) . "\n\n";
        return Command::SUCCESS;
    }

    private function generate_signature($request, $secret_key)
    {
        $devglan_request = [
            'algo' => 'SHA-256',
            'inputString' => json_encode($request),
            'outputFormat' => 'Base64',
            'secretKey' => $secret_key
        ];

        $response = Http::post('https://www.devglan.com/online-tools/hmac-sha256-online', $devglan_request);

        return json_decode($response->body())->outputString;

    }
}
