<?php

namespace App\Console\Commands;

use App\Models\OVO\Log;
use App\Models\TransactionV2;
use Illuminate\Console\Command;

class OVOCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ovo:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bad_datas = [];
        $good_datas = [];
        $pending_transactions = TransactionV2::where('state', 2)
            ->where('channel', 'OVO')
            ->whereDate('created_at', '<=', '2021-09-30')
            ->get();
        echo 'There are '. count($pending_transactions) . ' is requested data' . "\n";
        foreach ($pending_transactions as $pending_transaction) {
            $is_good_data = false;
            $middleware_order_id = $pending_transaction->order_id;
            $logs_data = Log::where('order_id', 'like', '%'.$middleware_order_id.'%')->get();
            foreach ($logs_data as $log) {
                if ($log->transaction_type == 'CREATE PTP' &&
                    $log->response_payload != null &&
                    str_contains($log->response_payload, 'transactionResponseData')) {
                    $is_good_data = true;
                    echo $pending_transaction->id . " is good because log: " . $log->response_payload . "\n";
                }
            }
            if (!$is_good_data) {
                array_push($bad_datas, $pending_transaction->id);
                $pending_transaction->state = 7;
                echo $pending_transaction->id . " is BAD because there is no payment detected on logs\n";
            } else {
                array_push($good_datas, $pending_transaction->id);
                $pending_transaction->state = 1;
                echo $pending_transaction->id . " is GOOD because there is payment detected on logs\n";
            }
            $pending_transaction->save();
        }

        echo "Bad data: \n";
        foreach ($bad_datas as $bad_data) {
            echo $bad_data .",";
        }
        echo "\n\nTotal bad data: ". count($bad_datas);
        echo "\n\nGood data: \n";
        foreach ($good_datas as $good_data) {
            echo $good_data . ",";
        }
        echo "\n\nTotal good data: ". count($good_datas) . "\n\n";
        return Command::SUCCESS;
    }
}
