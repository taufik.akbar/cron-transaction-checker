<?php

namespace App\Models\OVO;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $connection = 'pgsql2';
    use HasFactory;
}
