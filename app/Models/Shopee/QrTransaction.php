<?php

namespace App\Models\Shopee;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QrTransaction extends Model
{
    protected $connection = 'pgsql3';
    use HasFactory;
}
