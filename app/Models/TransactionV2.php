<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionV2 extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'transaction_v2';
    public $timestamps = false;
    use HasFactory;
}
